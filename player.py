import pygame

class Player:
    """Class to manage the player"""

    def __init__(self,mg):
        """Initialize the player and set starting position"""
        self.screen = mg.screen
        self.screen_rect = mg.screen.get_rect()

        self.image = pygame.image.load('images/ship.bmp')
        self.rect = self.image.get_rect()

        self.rect.center = self.screen_rect.center

    def blitme(self):
        """Draw the ship at its current location"""
        self.screen.blit(self.image, self.rect)