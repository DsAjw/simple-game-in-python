import sys
import pygame

from settings import Settings
from player import Player

class SpaceGame:
    """Overall class to manage game assets and behavior"""

    def __init__(self):
        """Initialize the game, and create game resources"""
        pygame.init()
        self.settings = Settings()
        self.screen = pygame.display.set_mode((self.settings.screen_width,self.settings.screen_height))
        pygame.display.set_caption("My Game")

        self.player = Player(self)

        self.bg_color = self.settings.bg_color

    def run_game(self):
        """Start the main loop for the game"""
        while True:
            self._check_events()
            self._update_screen()

            pygame.display.flip()

    def _check_events(self): # leading _ means its a helper method
        """Respond to keypresses and mouse events. """
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
    def _update_screen(self):
        """Update images on the screen, and flip to the new screen. """
        self.screen.fill(self.settings.bg_color)
        self.player.blitme()

if __name__ == '__main__':
    sg = SpaceGame()
    sg.run_game()